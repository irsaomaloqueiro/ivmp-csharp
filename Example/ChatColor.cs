﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example
{
    static class ChatColor
    {
        public static uint ERROR = 0xFFFF0000;
        public static uint SUCCESS = 0xFF00FF00;
        public static uint WHITE = 0xFFFFFFFF;
    }
}
